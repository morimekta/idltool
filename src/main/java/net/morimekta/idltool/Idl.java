/*
 * Copyright 2017 (c) Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.idltool;

import net.morimekta.console.args.ArgumentException;
import net.morimekta.console.args.ArgumentOptions;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Flag;
import net.morimekta.console.args.Option;
import net.morimekta.console.args.SubCommand;
import net.morimekta.console.args.SubCommandSet;
import net.morimekta.idltool.cmd.Command;
import net.morimekta.idltool.cmd.Help;
import net.morimekta.idltool.cmd.RemoteFetch;
import net.morimekta.idltool.cmd.RemoteList;
import net.morimekta.idltool.cmd.RemotePublish;
import net.morimekta.idltool.cmd.RemoteStatus;
import net.morimekta.idltool.cmd.RemoteUpdate;
import net.morimekta.providence.serializer.SerializerException;
import net.morimekta.util.FileUtil;
import net.morimekta.util.json.JsonException;
import net.morimekta.util.lexer.LexerException;
import net.morimekta.util.lexer.UncheckedLexerException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static net.morimekta.console.util.Parser.dirPath;

public class Idl {
    private Command command = null;
    private boolean help    = false;
    private boolean version = false;
    private boolean verbose = false;

    private Path         workingGitDir     = null;
    private Path         cacheDir          = null;
    private String       repository        = null;
    private List<String> extraRepositories = new ArrayList<>();
    private String       remoteName        = "origin";

    @Nonnull
    public Path getWorkingGitDir() throws IOException {
        if (workingGitDir == null) {
            Path pwd = FileUtil.readCanonicalPath(Paths.get(".").toAbsolutePath());
            workingGitDir = pwd;
            while (!Files.exists(workingGitDir.resolve(".idlrc"))) {
                Path parent = workingGitDir.getParent();
                if (parent == null ||
                        parent.getFileName() == null ||
                        parent.getFileName().toString().isEmpty()) {
                    throw new IOException("No .idlrc file at: " + pwd + " (recursive)");
                }
                workingGitDir = parent;
            }
        }
        return workingGitDir;
    }

    public String getRemoteName() {
        return remoteName;
    }

    // -------------- SET --------------

    private void setCommand(Command command) {
        this.command = command;
    }

    void setWorkingDir(Path pwd) {
        this.workingGitDir = pwd;
    }

    private void setCacheDir(Path cd) {
        this.cacheDir = cd;
    }

    private void setHelp(boolean help) {
        this.help = help;
    }

    private boolean showHelp() {
        return (help || command == null);
    }

    private void setVersion(boolean version) {
        this.version = version;
    }

    private boolean showVersion() {
        return version && !help;
    }

    private void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    private void setRepository(String repository) {
        this.repository = repository;
    }

    private void setRemoteName(String remoteName) {
        this.remoteName = remoteName;
    }

    private ArgumentParser makeParser() {
        ArgumentParser parser = new ArgumentParser("idl",
                                                   IdlUtils.versionString(),
                                                   "IDL replacement by morimekta",
                                                   ArgumentOptions.defaults()
                                                                  .withSubCommandsShown(true));

        parser.add(new Flag("--help", "h?", "Show help", this::setHelp, null));
        parser.add(new Flag("--version", "V", "Show program version", this::setVersion));
        parser.add(new Flag("--verbose", null, "Show verbose exceptions", this::setVerbose));

        parser.add(new Option("--remote",
                              "R",
                              "NAME",
                              "The remote name or URL to use for repository naming",
                              this::setRemoteName));
        parser.add(new Option("--pwd", null, "DIR", "The local git repository root directory", dirPath(this::setWorkingDir), "."));
        parser.add(new Option("--cache", null, "DIR", "The repository cache directory", dirPath(this::setCacheDir), "~/.cache/idltool"));

        parser.add(new Option("--repository",
                              "r",
                              "REPO",
                              "The main registry git repository",
                              this::setRepository));
        parser.add(new Option("--extra-repository",
                              "e",
                              "REPO",
                              "Extra registry repositories",
                              extraRepositories::add,
                              null,
                              true,
                              false,
                              false));

        SubCommandSet<Command> subCommandSet = new SubCommandSet<>("cmd", "The IDL command to run.", this::setCommand);
        subCommandSet.add(new SubCommand<>("help",
                                           "Show help information.",
                                           () -> new Help(subCommandSet, parser),
                                           Command::makeParser));
        subCommandSet.add(new SubCommand<>("list",
                                           "List available remotes.",
                                           () -> new RemoteList(parser),
                                           Command::makeParser));
        subCommandSet.add(new SubCommand<>("status",
                                           "List status changes to fetched remotes.",
                                           () -> new RemoteStatus(parser, false),
                                           Command::makeParser));
        subCommandSet.add(new SubCommand<>("diff",
                                           "List file changes to fetched remotes.",
                                           () -> new RemoteStatus(parser, true),
                                           Command::makeParser));
        subCommandSet.add(new SubCommand<>("update",
                                           "Update remote repositories.",
                                           () -> new RemoteUpdate(parser),
                                           Command::makeParser));
        subCommandSet.add(new SubCommand<>("fetch",
                                           "Fetch a remote repository.",
                                           () -> new RemoteFetch(parser),
                                           Command::makeParser));
        subCommandSet.add(new SubCommand<>("publish",
                                           "Publish local IDL to remote.",
                                           () -> new RemotePublish(parser),
                                           Command::makeParser));
        parser.add(subCommandSet);

        return parser;
    }

    void execute(String... args) {
        Locale.setDefault(Locale.US);  // just for the record.

        ArgumentParser parser = makeParser();

        try {
            parser.parse(args);

            if (showVersion()) {
                System.out.println(parser.getVersion());
                return;
            } else if (showHelp()) {
                System.out.println(parser.getProgramDescription());
                System.out.println("Usage: " + parser.getSingleLineUsage());
                System.out.println();
                parser.printUsage(System.out);

                if (command != null) {
                    ArgumentParser subParser = command.makeParser();
                    System.err.println();
                    System.err.println("Options for " + subParser.getProgram());
                    System.err.println();
                    subParser.printUsage(System.err);
                }
                return;
            }

            parser.validate();

            if (command instanceof Help) {
                command.execute(null);
            } else {
                command.execute(new IdlTool(this));
            }
            return;
        } catch (ArgumentException e) {
            System.err.println("Argument Error: " + e.getMessage());
            System.err.println();
            System.err.println("Usage: " + parser.getSingleLineUsage());
            System.err.println();
            parser.printUsage(System.err);

            if (e.getParser() != null && e.getParser() != parser) {
                ArgumentParser subParser = e.getParser();
                System.err.println();
                System.err.println("Options for " + subParser.getProgram() + ":");
                System.err.println();
                System.err.println("Usage: " + subParser.getSingleLineUsage());
                System.err.println();
                subParser.printUsage(System.err);
            }
            if (verbose) {
                System.err.println();
                e.printStackTrace();
            }
        } catch (LexerException e) {
            System.err.println(e.displayString());
            if (verbose) {
                System.err.println();
                e.printStackTrace();
            }
        } catch (UncheckedLexerException e) {
            System.err.println(e.displayString());
            if (verbose) {
                System.err.println();
                e.printStackTrace();
            }
        } catch (SerializerException e) {
            if (e.getCause() instanceof JsonException) {
                System.err.println(((JsonException) e.getCause()).displayString());
            } else {
                System.err.println(e.displayString());
            }
            if (verbose) {
                System.err.println();
                e.printStackTrace();
            }
        } catch (IOException | UncheckedIOException e) {
            System.err.println("I/O Error: " + e.getMessage());
            if (verbose) {
                System.err.println();
                e.printStackTrace();
            }
        } catch (Exception e) {
            System.err.println("Internal Error: " + e.getMessage());
            if (verbose) {
                System.err.println();
                e.printStackTrace();
            }
        }

        exit(1);
    }

    protected void exit(int i) {
        System.exit(i);
    }

    public static void main(String... args) {
        new Idl().execute(args);
    }

    public Path getCacheDir() {
        return cacheDir;
    }

    public String getRepository() {
        return repository;
    }

    public List<String> getExtraRepositories() {
        return extraRepositories;
    }
}
