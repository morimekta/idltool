/*
 * Copyright 2017 (c) Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.idltool.cmd;

import net.morimekta.console.args.ArgumentException;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Option;
import net.morimekta.console.chr.Color;
import net.morimekta.console.util.Parser;
import net.morimekta.idltool.IdlTool;
import net.morimekta.idltool.meta.Meta;
import net.morimekta.idltool.meta.Remote;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static net.morimekta.idltool.IdlUtils.formatAgo;

/**
 * Interactively manage branches.
 */
public class RemoteList extends Command {
    public RemoteList(ArgumentParser parent) {
        super(parent);
    }

    public enum Sorting {
        alphabetical,
        newest_first,
        oldest_first,
    }

    private Sorting sorting = Sorting.alphabetical;

    private void setSorting(Sorting sorting) {
        this.sorting = sorting;
    }

    @Override
    public ArgumentParser makeParser() {
        ArgumentParser parser = new ArgumentParser(getParent(), "list",
                                                   "List available remotes and last update for each.");

        parser.add(new Option("--sorting", "s", "SORT",
                              "Sorting of the remotes, one of [alphabetical, newest_first, oldest_first]",
                              Parser.oneOf(Sorting.class, this::setSorting), Sorting.alphabetical.name()));

        return parser;
    }

    private Comparator<Map.Entry<String, Remote>> comparator() {
        switch (sorting) {
            case alphabetical:
                return Comparator.comparing(Map.Entry::getKey);
            case newest_first:
                return Comparator.comparing(e -> -e.getValue().getTime());
            case oldest_first:
                return Comparator.comparing(e -> e.getValue().getTime());
        }
        throw new ArgumentException("Wooops!");
    }

    @Override
    public void execute(IdlTool idlTool) {
        Meta localMeta = idlTool.getLocalMeta();

        boolean first = true;

        for (String repository : idlTool.getRepositories()) {
            try {
                Meta meta = idlTool.getCache().getRepository(repository).getMeta();
                int longestRemote = Math.max(30,
                                             meta.getRemotes()
                                                 .keySet()
                                                 .stream()
                                                 .mapToInt(String::length)
                                                 .max()
                                                 .orElse(0));

                if (first) {
                    first = false;
                } else {
                    System.out.println();
                }

                if (meta.getRemotes().isEmpty()) {
                    System.out.println(format("%s%s%s is empty.", new Color(Color.YELLOW, Color.BOLD), repository, Color.CLEAR));
                    continue;
                }

                System.out.println(format("%s%s%s", Color.GREEN, repository, Color.CLEAR));
                System.out.println(format("%s%s - %s - %s%s",
                                          new Color(Color.YELLOW, Color.BOLD),
                                          StringUtils.center("<<-- remote -->>    ", longestRemote),
                                          StringUtils.center("remote date", 19),
                                          StringUtils.center("local date", 19),
                                          Color.CLEAR));

                for (Map.Entry<String, Remote> remoteEntry : meta.getRemotes()
                                                                 .entrySet()
                                                                 .stream()
                                                                 .sorted(comparator())
                                                                 .collect(Collectors.toList())) {
                    Remote local = localMeta.getRemotes().get(remoteEntry.getKey());

                    boolean change = false;
                    if (local != null && remoteEntry.getValue().getTime() != local.getTime()) {
                        change = true;
                    }

                    System.out.println(format("%s%s - %s - %s%s",
                                              change ? Color.YELLOW : Color.CLEAR,
                                              StringUtils.rightPad(remoteEntry.getKey(), longestRemote),
                                              StringUtils.rightPad(formatAgo(remoteEntry.getValue().getTime()), 19),
                                              local == null ? "" : formatAgo(local.getTime()),
                                              Color.CLEAR));
                }
            } catch (Exception e) {
                System.out.println("Bad repository " + repository + ": " + e.getMessage());
            }
        }
    }
}
