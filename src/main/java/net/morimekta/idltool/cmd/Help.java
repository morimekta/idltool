/*
 * Copyright 2017 (c) Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.idltool.cmd;

import net.morimekta.console.args.Argument;
import net.morimekta.console.args.ArgumentException;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.SubCommandSet;
import net.morimekta.console.chr.Color;
import net.morimekta.idltool.IdlTool;
import net.morimekta.util.Strings;

/**
 * The 'usage' sub-command.
 */
public class Help extends Command {
    private final SubCommandSet<Command> subCommandSet;
    private       String                 command;

    public Help(SubCommandSet<Command> subCommandSet, ArgumentParser parent) {
        super(parent);
        this.subCommandSet = subCommandSet;
    }

    @Override
    public ArgumentParser makeParser() {
        ArgumentParser parser = new ArgumentParser(getParent(), "help", "Shows help.");

        parser.add(new Argument("command", "Show help for given command", this::setCommand, "none"));

        return parser;
    }

    private void setCommand(String s) {
        this.command = s;
    }

    @Override
    public void execute(IdlTool idlTool) {
        if (command != null) {
            System.out.println("Usage: " + subCommandSet.getSingleLineUsage(command));
            System.out.println();
            subCommandSet.printUsage(System.out, command);
            System.out.println();
            switch (command) {
                case "help":
                    System.out.println("Prints help for a specific sub-command or for idl tool as a whole.");
                    System.out.println("Example: $ idl help      - Print this help info.");
                    System.out.println("Example: $ idl help list - Print help about the 'list' command.");
                    break;
                case "list":
                    System.out.println("Example: $ idl list");
                    System.out.println(Color.GREEN +
                                       "git@github.com/morimekta/idl-registry.git" +
                                       Color.CLEAR);
                    System.out.println(new Color(Color.BOLD, Color.YELLOW) +
                                       "       <<-- remote -->>         -      remote date    -      local date" +
                                       Color.CLEAR);
                    System.out.println("github.com/morimekta/providence - 2017-03-17 14:26:51 -");
                    System.out.println("github.com/morimekta/idltool    - 2017-03-17 12:12:00 -");
                    System.out.println("github.com/zedge/thrift-any     - 2016-12-01 12:57:52 -");
                    break;
                case "status":
                    System.out.println("Example: $ idl status");
                    System.out.println(Color.BOLD +
                                       "Updated on remote github.com/morimekta/idltool" +
                                       Color.CLEAR);
                    System.out.format("  %s%s%s   (%s-23%s)\n",
                                      Color.YELLOW, "config.thrift", Color.CLEAR,
                                      Color.RED, Color.CLEAR);
                    System.out.format("+ %s%s%s   (%s+144%s)\n",
                                      Color.GREEN, "meta.thrift  ", Color.CLEAR,
                                      Color.GREEN, Color.CLEAR);
                    break;
                case "diff":
                    int diffSeparatorLength = 72;

                    System.out.println("Example: $ idl diff");
                    System.out.println(Color.BOLD +
                                       "Updated on remote github.com/morimekta/idltool" +
                                       Color.CLEAR);
                    System.out.println();
                    System.out.println(Color.DIM + Strings.times("#", diffSeparatorLength) + Color.CLEAR);
                    System.out.format("  %s%s%s   (%s-2%s)\n",
                                      Color.YELLOW, "local github.com/morimekta/idltool/config.thrift", Color.CLEAR,
                                      Color.RED, Color.CLEAR);
                    System.out.println(Color.DIM + Strings.times("-", diffSeparatorLength) + Color.CLEAR);
                    System.out.println(new Color(Color.CYAN, Color.DIM) + "@@ -5 +5 @@" +
                                       Color.CLEAR + Color.DIM + " -- (skipped 4 lines)" + Color.CLEAR);
                    System.out.println("  # Same comment, not important.");
                    System.out.println("  #  ...");
                    System.out.println("  #  ... ditto.");
                    System.out.format("%s- removed line 1%s%n", Color.RED, Color.CLEAR);
                    System.out.format("%s- removed line 2%s%n", Color.RED, Color.CLEAR);
                    System.out.println("  same same");
                    System.out.println("  more of same");
                    System.out.println(Color.DIM + Strings.times("#", diffSeparatorLength) + Color.CLEAR);
                    System.out.println();
                    System.out.println(Color.DIM + Strings.times("#", diffSeparatorLength) + Color.CLEAR);
                    System.out.format("  %s%s%s   (%s+4%s) %sNew%s\n",
                                      Color.YELLOW, "local github.com/morimekta/idltool/meta.thrift  ", Color.CLEAR,
                                      Color.GREEN, Color.CLEAR,
                                      Color.GREEN, Color.CLEAR);
                    System.out.println(Color.DIM + Strings.times("-", diffSeparatorLength) + Color.CLEAR);
                    System.out.format("%s+ # important comment%s%n", Color.GREEN, Color.CLEAR);
                    System.out.format("%s+ added line 1%s%n", Color.GREEN, Color.CLEAR);
                    System.out.format("%s+ added line 2%s%n", Color.GREEN, Color.CLEAR);
                    System.out.format("%s+ added line 3%s%n", Color.GREEN, Color.CLEAR);
                    System.out.println(Color.DIM + Strings.times("#", diffSeparatorLength) + Color.CLEAR);
                    break;
                case "update":
                    System.out.println("Example: $ idl update");
                    break;
                case "fetch":
                    System.out.println("Example: $ idl fetch github.com/morimekta/idltool");
                    break;
                case "publish":
                    System.out.println("Example: $ idl publish");
                    break;
                default:
                    throw new ArgumentException("Help for " + command + " not created!");
            }
        } else {
            ArgumentParser parser = getParent();
            System.out.println(parser.getProgramDescription());
            System.out.println("Usage: " + parser.getSingleLineUsage());
            System.out.println();
            parser.printUsage(System.out);
        }
    }
}
