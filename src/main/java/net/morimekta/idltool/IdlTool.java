package net.morimekta.idltool;

import net.morimekta.idltool.git.Repository;
import net.morimekta.idltool.git.RepositoryCache;
import net.morimekta.idltool.meta.Meta;
import net.morimekta.util.collect.UnmodifiableSet;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Optional;
import java.util.Set;

public class IdlTool {
    private final Meta localMeta;
    private final String localRemoteName;


    private final Repository local;
    private final RepositoryCache cache;
    private final String upstreamRepository;
    private final Set<String> repositories;

    IdlTool(Idl idl) throws IOException {
        this.local = new Repository(idl.getWorkingGitDir());
        this.cache = new RepositoryCache(IdlUtils.findHomeDotCacheIdlTool(idl.getCacheDir()));

        this.localMeta = readLocalMeta();
        this.localRemoteName = IdlUtils.getLocalRemoteName(
                local,
                Optional.ofNullable(idl.getRemoteName())
                        .orElseGet(() -> {
                            try {
                                return local.getRC().getRemote();
                            } catch (IOException e) {
                                throw new UncheckedIOException(e.getMessage(), e);
                            }
                        }));
        this.upstreamRepository = Optional.ofNullable(idl.getRepository())
                                          .orElseGet(() -> {
                                              try {
                                                  return local.getRC().getRepository();
                                              } catch (IOException e) {
                                                  throw new UncheckedIOException(e.getMessage(), e);
                                              }
                                          });
        UnmodifiableSet.Builder<String> repositories = UnmodifiableSet.builder();
        repositories.add(upstreamRepository);
        repositories.addAll(idl.getExtraRepositories());
        try {
            repositories.addAll(local.getRC().getExtraRepositories());
        } catch (IOException ignore) {}
        this.repositories = repositories.build();
    }

    @Nonnull
    public Meta getLocalMeta() {
        return localMeta;
    }

    @Nonnull
    public String getLocalRemoteName() {
        return localRemoteName;
    }

    public RepositoryCache getCache() {
        return cache;
    }

    @Nonnull
    public Repository getRemoteRepository(String remoteName) throws IOException {
        return cache.getRepository(getRemoteRepositoryUrlIsh(remoteName));
    }

    @Nonnull
    private String getRemoteRepositoryUrlIsh(String remoteName) throws IOException {
        if (localRemoteName.equals(remoteName)) {
            return upstreamRepository;
        }
        for (String repository : repositories) {
            Meta meta = cache.getRepository(repository).getMeta();
            if (meta.getRemotes().containsKey(remoteName)) {
                return repository;
            }
        }
        throw new IllegalStateException("No repository for remote " + remoteName);
    }

    private Meta readLocalMeta() {
        try {
            return local.getMeta();
        } catch (IOException e) {
            return Meta.builder().build();
        }
    }

    public Set<String> getRepositories() {
        return repositories;
    }

    public Repository getLocal() {
        return local;
    }
}
