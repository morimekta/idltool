package net.morimekta.idltool.git;

import org.junit.Test;

import java.util.regex.Matcher;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RepositoryTest {
    @Test
    public void testPatterns() {
        assertStatus("## master", "master", null);
        assertStatus("## master...origin/master", "master", "origin/master");
        assertStatus("## master...origin/master (ahead 2)", "master", "origin/master");
        assertStatus("## HEAD (no branch)", "HEAD", null);

        assertFile("M  foo/bar.txt", "foo/bar.txt", "M ");
        assertFile("?? foo/bar.txt", "foo/bar.txt", "??");
        assertFile(" A bar.txt", "bar.txt", " A");
    }

    private void assertStatus(String status, String branch, String remote) {
        Matcher matcher = Repository.STATUS.matcher(status);
        assertThat(matcher.matches(), is(true));
        assertThat(matcher.group("branch"), is(branch));
        assertThat(matcher.group("remote"), is(remote));
    }

    private void assertFile(String line, String file, String status) {
        Matcher matcher = Repository.FILE.matcher(line);
        assertThat(matcher.matches(), is(true));
        assertThat(matcher.group("file"), is(file));
        assertThat(matcher.group("status"), is(status));
    }
}
