package net.morimekta.idltool.git;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Path;
import java.util.Map;

public class Status {
    public final String branch;
    public final String remote;
    public final Map<Path, String> files;

    public Status(@Nonnull String branch,
                  @Nullable String remote,
                  @Nonnull Map<Path, String> files) {
        this.branch = branch;
        this.remote = remote;
        this.files = files;
    }
}
