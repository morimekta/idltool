FROM java:openjdk-8-jre-alpine

RUN apk add --no-cache openssh-client git bash curl

COPY src/deb/bin/idl /usr/bin/
COPY target/idltool.jar /usr/share/idltool/

ENTRYPOINT ["idl"]
CMD []