package net.morimekta.idltool.git;

import net.morimekta.idltool.config.IdlRC;
import net.morimekta.idltool.meta.Meta;
import net.morimekta.idltool.meta.Remote;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.util.FileUtil;
import net.morimekta.util.collect.UnmodifiableMap;
import net.morimekta.util.collect.UnmodifiableSortedMap;
import net.morimekta.util.concurrent.ProcessExecutor;
import org.apache.commons.codec.digest.DigestUtils;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Clock;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.idltool.config.Idl_Constants.IDL_RC;
import static net.morimekta.idltool.config.Idl_Constants.META_JSON;

public class Repository {
    static final Pattern STATUS = Pattern.compile("## (?<branch>[_a-zA-Z0-9]+([-./][_a-zA-Z0-9]+)*)([.][.][.](?<remote>[-_/a-zA-Z0-9]+))?( .*)?");
    static final Pattern FILE = Pattern.compile("(?<status>..) (?<file>.*)");
    static final Pattern REMOTE = Pattern.compile("(?<remote>[-_a-zA-Z0-9]+)\\s+(?<url>.+)\\s+\\((?<type>push|fetch)\\)");
    private static final JsonSerializer JSON = new JsonSerializer().pretty();

    private final Path root;
    private IdlRC rc;
    private Path metaFile;
    private Meta meta;
    private Status initialStatus;
    private Map<String, String> remotes;

    public Repository(@Nonnull Path path) throws IOException {
        Path root = path.toAbsolutePath();
        while (!Files.isDirectory(root.resolve(".git"))) {
            if (root.getParent() == null ||
                root.getParent().getFileName() == null ||
                root.getParent().getFileName().toString().isEmpty()) {
                throw new IOException("No git repo in path: " + path.toString());
            }
        }
        this.root = root;
        this.initialStatus = getUpdatedStatus();

        Path idlRc = root.resolve(IDL_RC);
        if (Files.exists(idlRc)) {
            try (Reader reader = new BufferedReader(
                    new FileReader(idlRc.toFile()))) {
                rc = JSON.deserialize(reader, IdlRC.kDescriptor);
            }
        } else {
            rc = IdlRC.builder().build();
        }
    }

    public Path getRoot() {
        return root;
    }

    public Status getInitialStatus() {
        return initialStatus;
    }

    public Map<String, String> getRemotes() throws IOException {
        if (remotes == null) {
            Map<String, String> remotes = new TreeMap<>();
            String[] lines = run("git", "remote", "-v").split("\r?\n");

            for (String line : lines) {
                Matcher remote = REMOTE.matcher(line);
                if (!remote.matches()) {
                    System.err.println("NOT: " + line);
                    continue;
                }
                String name = remote.group("remote");
                if ("push".equals(remote.group("type")) || !remotes.containsKey(name)) {
                    remotes.put(name, remote.group("url"));
                }
            }

            this.remotes = UnmodifiableSortedMap.copyOf(remotes);
        }
        return remotes;
    }

    public Status getUpdatedStatus() throws IOException {
        String[] status = run("git", "status", "--porcelain", "--branch").split("\r?\n");

        Matcher matcher = STATUS.matcher(status[0]);
        if (!matcher.matches()) {
            throw new IOException("Unrecognized first line of git status: " + status[0]);
        }

        Map<Path, String> files = new TreeMap<>();
        for (int i = 1; i < status.length; ++i) {
            if (status[i].isEmpty()) continue;

            Matcher file = FILE.matcher(status[i]);
            if (!file.matches()) {
                throw new IOException("Unrecognized status line: " + status[i]);
            }
            files.put(Paths.get(file.group("file")), file.group("status"));
        }
        return new Status(matcher.group("branch"),
                matcher.group("remote"),
                UnmodifiableMap.copyOf(files));
    }

    public Path getMetaFile(boolean create) throws IOException {
        if (metaFile == null) {
            Path alt1 = root
                    .resolve(Paths.get(rc.getIdlLocation()))
                    .resolve(META_JSON);
            if (Files.isRegularFile(alt1)) {
                metaFile = alt1;
            } else {
                Path alt2 = root.resolve(META_JSON);
                if (Files.isRegularFile(alt2)) {
                    metaFile = alt2;
                } else if (create) {
                    metaFile = alt1;
                    Files.write(metaFile, "{}\n".getBytes(UTF_8), StandardOpenOption.CREATE);
                } else {
                    throw new IOException("No meta file in repo " + root.toString());
                }
            }
        }
        return metaFile;
    }

    @Nonnull
    public Path remoteFilesLocation(String remote, boolean create) throws IOException {
        Path location = root.resolve("idl").resolve(remote);

        if (!Files.exists(location) && create) {
            Files.createDirectories(location);
        }

        return location;
    }

    @Nonnull
    public Remote buildRemote(String remote) throws IOException {
        // idl / remote / *
        Path location = remoteFilesLocation(remote, false);
        if (!Files.exists(location)) {
            return Remote.builder().build();
        }

        Remote._Builder builder = Remote.builder();
        for (Path path : FileUtil.list(location, true)) {
            if (Files.isHidden(path)) continue;

            String sha1sum = readShaSum(path);
            Path relative = location.relativize(path);
            builder.putInShasums(relative.toString(), sha1sum);
        }

        long timestamp = Clock.systemUTC().millis();
        builder.setTime(timestamp)
                .setVersion(timestamp);

        return builder.build();
    }

    @Nonnull
    private String readShaSum(Path path) throws IOException {
        return DigestUtils.sha1Hex(Files.readAllBytes(path));
    }

    public Meta getMeta() throws IOException {
        if (meta == null) {
            meta = readMetaFile(getMetaFile(false));
        }
        return meta;
    }

    public Repository putMeta(Meta meta) throws IOException {
        try (PrintWriter writer = new PrintWriter(
                new BufferedWriter(
                        new FileWriter(getMetaFile(true).toFile())))) {
            JSON.serialize(writer, meta);
            writer.println();
        }
        return this;
    }

    public IdlRC getRC() throws IOException {
        return rc;
    }

    public Repository fetch() throws IOException {
        run("git", "fetch", "origin", "-p");
        return this;
    }

    public Repository reset() throws IOException {
        run("git", "reset", "origin/master", "--hard");
        return this;
    }

    public Repository add() throws IOException {
        run("git", "add", "-A", ".");
        return this;
    }

    public Repository commit(String message) throws IOException {
        run("git", "commit", "-m", message);
        return this;
    }

    public Repository push() throws IOException {
        run("git", "push");
        return this;
    }

    private Meta readMetaFile(Path path) throws IOException {
        try (FileInputStream fis = new FileInputStream(path.toFile());
             InputStreamReader reader = new InputStreamReader(fis)) {
            return JSON.deserialize(reader, Meta.kDescriptor);
        }
    }

    private String run(String... cmd) throws IOException {
        ProcessExecutor executor = new ProcessExecutor(cmd);
        executor.setDirectory(root.toFile());
        executor.setDeadlineMs(60000L);
        if (executor.call() != 0) {
            throw new IOException("Failed to run " + Arrays.toString(cmd) + ": " + executor.getError().split("\r?\n")[0]);
        }
        return executor.getOutput();
    }
}
