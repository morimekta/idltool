namespace java net.morimekta.idltool.config

const string IDL_RC         = ".idlrc"
const string IDL            = "idl"
const string META_JSON      = "meta.json"

/**
 * Content of the '.idlrc' file for the current project.
 */
struct IdlRC {
    /**
     * Git repository location holding the IDL file registry. Must be a valid
     * git remote URI, e.g. in form of "git@github.com:user/repository.git" or
     * "https://github.com/user/repository.git"
     */
    1: required string repository;

    /**
     * In addition to the main repository, you can fetch updates from other
     * repositories too. Note that the fetched directory does not care about
     * which repository it was fetched from. So the first repository in
     * [repository *extra_repositories] that contains a remote, will use that
     * to fetch.
     */
    2: optional list<string> extra_repositories = [];

    /**
     * Relative location of the IDL remotes. Defaults to 'idl'.
     */
    3: optional string idl_location = "idl";

    /**
     * Optional remote name to override reading from git remote config. This
     * can be used to use a different directory for representing the remote
     * remotely.
     */
    4: optional string remote;

    // /**
    //  * Optional remote name to override reading from git remote config. This
    //  * can ge used to use a different directoy for representing the remote
    //  * locally without interfering with the remote name used in the
    //  * repository.
    //  *
    //  * If it begins with '/' will be from the root of the git repository,
    //  * otherwise from the 'idl_location' directory.
    //  */
    // 5: optional string local_remote;
}
