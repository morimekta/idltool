/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.idltool;

import net.morimekta.idltool.git.Repository;
import net.morimekta.idltool.git.Status;
import net.morimekta.util.FileUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

/**
 * General IDL tool utilities.
 */
public class IdlUtils {
    static String versionString() {
        Properties properties = new Properties();
        try (InputStream in = IdlUtils.class.getResourceAsStream("/build.properties")) {
            properties.load(in);
            return "v" + properties.getProperty("build.version");
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    static Path findHomeDotCacheIdlTool(Path cacheOverride) throws IOException {
        if (cacheOverride != null) {
            cacheOverride = FileUtil.readCanonicalPath(cacheOverride);
            if (Files.isDirectory(cacheOverride)) {
                return cacheOverride;
            }
            throw new IOException("No such cache directory: " + cacheOverride.toString());
        }

        Path home = FileUtil.readCanonicalPath(Paths.get(System.getenv("HOME")).toAbsolutePath());
        cacheOverride = home.resolve(Paths.get(".cache", "idltool"));
        if (!Files.exists(cacheOverride)) {
            Files.createDirectories(cacheOverride);

        }
        if (!Files.isDirectory(cacheOverride)) {
            throw new IOException("RC location is not a directory: " + cacheOverride.toString());
        }
        return cacheOverride;
    }

    static String getLocalRemoteName(Repository local, String remoteName) throws IOException {
        // If the remote name is a URL (https:// or git@), just treat the remote name as the URL.
        if (remoteName != null &&
            (remoteName.startsWith("https://") || remoteName.startsWith("git@"))) {
            return idlRemotePathFromUrl(remoteName);
        }

        Status status = local.getInitialStatus();
        if (remoteName == null && status.remote != null) {
            remoteName = status.remote.split("/")[0];
        }
        String url = local.getRemotes().get(remoteName);
        if (url == null) {
            url = local.getRemotes().values().stream().findFirst().orElse(null);
        }
        if (url == null) {
            throw new IOException("No remote in git repo " + local.getRoot().toString());
        }
        return idlRemotePathFromUrl(url);
    }

    private static String idlRemotePathFromUrl(String remoteUrl) {
        return remoteUrl.replaceAll("^.*://", "")
                        .replaceAll("^.*@", "")
                        .replaceAll("[.]git$", "")
                        .replaceAll(":", "/");
    }

    public static String formatAgo(long timestamp) {
        Instant instant = Instant.ofEpochSecond(timestamp / 1000);
        LocalDateTime local = instant.atZone(Clock.systemUTC().getZone())
                                     .withZoneSameInstant(Clock.systemDefaultZone().getZone())
                                     .toLocalDateTime();
        return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(local).replaceAll("[T]", " ");
    }
}
