namespace java net.morimekta.idltool.meta

/**
 * Definition of a service remote.
 */
struct Remote {
    /**
     * The checked in timestamp for the last update of the program.
     */
    1: optional i64 time;

    /**
     * The checked in timestamp for the last update of the program.
     */
    2: optional i64 version;

    /**
     * The SHA-1 hash of the file content at the last update.
     *
     * Map from file name to sha sum.
     */
    3: optional map<string,string> shasums = {} (container = "SORTED");
}

/**
 * Content of the complete meta structure. Found in the 'idl/meta.json' file.
 */
struct Meta {
    /**
     * ISO timestamp of last update.
     */
    1: optional string time;

    /**
     * Millisecond timestamp of last update.
     */
    2: optional i64 version;

    /**
     * Map of repository location (host/path) to remote definition.
     */
    3: optional map<string,Remote> remotes = {} (container = "SORTED");
}
