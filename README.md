IDL Tools
=========

IDL tools is a java port of Über's [node based IDL tool](https://github.com/uber-node/idl),
which I kinda found problematic in many ways, mostly in the total useless error
handling.

The tool's basic job is to manage IDL files located in special folders in the
local GIT repository, and uses a set of commands to synchronize the content of
those folders with a remote IDL registry. The IDL files can be any non-special
non-hidden file. The hidden file criteria should be platform independent, so no
non-IDL files are synced.

In order to use the IDL tool, the git repository must have a `.idlrc` file in
it's root directory. All other files will be generated on demand, but here is
an overview of the structure.

* **[.idlrc]**: A JSON file specifying which GIT repository is used as the IDL
  registry. The registry must have a `meta.json` file in it's root folder.
* **[idl/meta.json]**: Is a JSON file containing meta information about the
  remotes that are fetched and synced to the local repository.
* **[idl/github.com/username/repository]** or **[idl/git.example.net/repository]**
  are local or remote IDL repositories that are synced with the local repository.
  The local (current) repository's files must be placed in a directory that
  matches its git repository location.

## Installation

You can find [installation instructions here](https://morimekta.net/versions.html),
available for brew (linux & mac), RPM and DEB (linux). There is also a
[tar.gz available here](https://bintray.com/morimekta/archive/idltool).

## The IDL commands

All the commands!

### idl list

The `idl list` command shows a list of all the remotes available, with timestamp
of last change. If the remote has a local copy, the last update of that copy
is also shown. E.g.:

```
$ idl list
https://github.com/morimekta/idl-registry.git
      <<-- remote -->>            -     remote date     -     local date
git.morimekta.net/my_private_repo - 2017-02-01 11:09:48 -
github.com/morimekta/providence   - 2017-09-05 11:47:57 - 2017-09-05 11:47:57
```

### idl fetch

The `idl fetch` command fetches or updates a specific remote. Which means that
what ever is the remote content of the IDL registry will override the local
content for **this remote only**. If the remote is not already in the 'synced'
remote list, it will be added.

### idl update

The `idl update` command updates all locally registered remotes, with the
explicit exception of the local repository.

### idl publish

The `idl publish` command is the only command that changes the content of the
registry. This will take any changes of the local respository's IDL files
and update the registry files to match, including adding missing files and
removing deleted files. This will publish to the main repository only.

#### idl status

The `idl status` command shows which change stats both on the local repository and
the remote repositories will be done with `idl update` and `idl publish` commands.

#### idl diff

The `idl diff` command shows which changes both on the local repository and
the remote repositories will be done with `idl update` and `idl publish` commands
including the actual file diff, not just the statistics.

### idl help

Shows help about how to use the idl tool. Should be pretty self-explanatory.

## Comparison to Original

This IDL tool contains some utilities that the original Über IDL tool did not
at the time of porting.

- Multiple repositories. This is done by adding the `extra_repositories`
  list to the `.idlrc` file, like:

    ```json
    {
      "repository": "git@github.com:morimekta/idl-registry.git",
      "extra_repositories": [
        "git@github.com:zedge/thrift-registry.git"
      ]
    }
    ```

  IDL files will be pulled from and updated form the extra repositories,
  but will not publish to, only the main "repository" will be affected
  on `publish`.

## Future enhancements

- Be able to specify what the local repository's remote name is in the .idlrc
  file, this way the repository location, can change without changing what
  remote name is matched with it.
- Be able to specify remote branch to sync toward on the registry. This way
  the caller of the update methods does not need to fork the registry in it's
  entirety, but can just make a branch on the registry repo instead.
- Make optional publish guard when publishing to the registry 'master' branch.

## Release

The first set of commands will build the actual release

```bash
# Do the maven release:
mvn release:prepare
mvn release:perform
mvn release:clean
git fetch origin
```

The last part will upload the generated binaries to bintray and make it
available for download and install for RedHat (yum), Debian (deb), MacOS (brew)
and to use in pipelines (docker).

```bash
./upload-bintray.sh
# And follow steps to publish new artifacts.
```
