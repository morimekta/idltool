/*
 * Copyright 2017 (c) Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.idltool.cmd;

import net.morimekta.console.args.Argument;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.idltool.IdlTool;
import net.morimekta.idltool.config.Idl_Constants;
import net.morimekta.idltool.git.Repository;
import net.morimekta.idltool.meta.Meta;
import net.morimekta.idltool.meta.Remote;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Interactively manage branches.
 */
public class RemoteFetch extends Command {
    public RemoteFetch(ArgumentParser parent) {
        super(parent);
    }

    private List<String> remoteNames = new ArrayList<>();

    private void addRemote(String remote) {
        this.remoteNames.add(remote);
    }

    @Override
    public ArgumentParser makeParser() {
        ArgumentParser parser = new ArgumentParser(getParent(), "fetch",
                                                   "Fetch a specific remote. It will add that remote to the list " +
                                                   "of remotes that is kept up to date.");

        parser.add(new Argument("remoteName",
                                "Remote path, as in the list",
                                this::addRemote,
                                null,
                                null,
                                true,
                                true,
                                false));

        return parser;
    }

    @Override
    public void execute(IdlTool idlTool) throws IOException {
        Meta localMeta = idlTool.getLocalMeta();
        Meta._Builder localMetaBuilder = localMeta.mutate();


        Path localIDL = idlTool.getLocal()
                               .getRoot()
                               .resolve(Idl_Constants.IDL);

        if (!Files.exists(localIDL)) {
            Files.createDirectories(localIDL);
            System.out.println("Created: " + localIDL);
        }

        for (String remoteName : remoteNames) {
            if (remoteName == null || remoteName.isEmpty()) {
                throw new IllegalArgumentException("Null or empty remote name");
            }
            if (idlTool.getLocalRemoteName().equals(remoteName)) {
                throw new IllegalArgumentException("Fetching local remote");
            }

            Repository repository = idlTool.getRemoteRepository(remoteName);
            Meta remoteMeta = repository.getMeta();
            Path remoteFilesLocation = repository.remoteFilesLocation(remoteName, false);
            if (!Files.exists(remoteFilesLocation)) {
                throw new IllegalStateException("No remote dir for " + remoteName +
                        " in " + repository.getRoot().toString());
            }
            Path localFilesLocation = idlTool.getLocal().remoteFilesLocation(remoteName, true);

            Remote remote = remoteMeta.getRemotes().get(remoteName);
            for (Map.Entry<String, String> entry : remote.getShasums().entrySet()) {
                Path source = remoteFilesLocation.resolve(entry.getKey());
                Path target = localFilesLocation.resolve(entry.getKey());

                Files.createDirectories(target.getParent());
                try {
                    Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    throw new IOException("Unable to copy " + source + " to " + target, e);
                }
            }

            Remote localRemote = idlTool.getLocal().buildRemote(remoteName);
            for (String file : localRemote.getShasums().keySet()) {
                if (!remote.getShasums().containsKey(file)) {
                    Path target = localFilesLocation.resolve(file);
                    Files.delete(target);
                }
            }

            localMetaBuilder.putInRemotes(remoteName, remote);
        }

        Instant now = Clock.systemUTC().instant();
        localMetaBuilder.setTime(DateTimeFormatter.ISO_ZONED_DATE_TIME.format(now.atZone(ZoneOffset.UTC)));
        localMetaBuilder.setVersion(now.toEpochMilli());

        idlTool.getLocal().putMeta(localMetaBuilder.build());
    }
}
