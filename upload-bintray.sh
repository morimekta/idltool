#!/usr/bin/env bash

set -e

function __sort() {
   type -fp gsort >/dev/null && command gsort "$@" || sort "$@"
}

export IDL_VERSION=$(git tag | grep '^v[0-9]\{1,4\}[.][0-9]\+' | __sort -V | tail -n 1 | cut -b '2-')

export deb="$(echo target/idltool-${IDL_VERSION}_all.deb | sed 's/-beta/~beta/')"
export rpm="target/rpm/idltool/RPMS/noarch/idltool-${IDL_VERSION}-1.noarch.rpm"
export tgz="target/idltool-${IDL_VERSION}.tar.gz"

echo "This will copy files to dl.morimekta.net:"
echo
echo "($(ls -lh ${tgz} | cut -d ' ' -f '5')) ${tgz}"
echo "($(ls -lh ${deb} | cut -d ' ' -f '5')) ${deb}"
echo "($(ls -lh ${rpm} | cut -d ' ' -f '5')) ${rpm}"
echo

echo "PS: Login to docker with:"
echo "docker login registry.gitlab.com"
echo

CONFIRM=
echo
echo -n "Continue? (y/N):"
read -n 1 CONFIRM
echo
if [[ "$CONFIRM" != "y" ]]
then
    exit 0
fi

docker build . \
       -t registry.gitlab.com/morimekta/idltool:${IDL_VERSION} \
       -t registry.gitlab.com/morimekta/idltool:latest
docker push registry.gitlab.com/morimekta/idltool:latest
docker push registry.gitlab.com/morimekta/idltool:${IDL_VERSION}

if [[ -d "../dl.morimekta.net/dl" ]]
then

  echo cp ${tgz} ../dl.morimekta.net/dl/archive/
  cp ${tgz} ../dl.morimekta.net/dl/archive/
  echo cp ${deb} ../dl.morimekta.net/dl/deb/
  cp ${deb} ../dl.morimekta.net/dl/deb/
  echo cp ${rpm} ../dl.morimekta.net/dl/rpm/
  cp ${rpm} ../dl.morimekta.net/dl/rpm/

else

  echo "copy files shown above to correct folder in dl.morimekta.net"
  echo

fi

SHA256SUM=$(cat ${tgz} | sha256sum | sed 's/ .*//')

if [[ -f "../homebrew-tools/Formula/idltool.rb" ]]
then

cat <<EOF > ../homebrew-tools/Formula/idltool.rb
class Idltool < Formula
    desc "IDL Tool"
    homepage "https://www.morimekta.net"
    version "${IDL_VERSION}"
    url "https://dl.morimekta.net/archive/idltool-#{version}.tar.gz"
    sha256 "${SHA256SUM}"

    depends_on :java => "1.8+"

    def install
        bin.install Dir["bin/*"]
        share.install Dir["share/*"]
    end
end
EOF

    echo
    echo "And go to ../homebrew-tools and commit and push changes."
else
    echo
    echo "Please go to 'homebrew-tools/Formula/idltool.rb' and set:"
    echo "    version ${IDL_VERSION}"
    echo "    sha256 ${SHA256SUM}"
fi

echo
