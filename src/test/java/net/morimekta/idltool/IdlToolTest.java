/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.idltool;

import net.morimekta.testing.rules.ConsoleWatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class IdlToolTest {
    @Rule
    public TemporaryFolder tmp     = new TemporaryFolder();
    @Rule
    public ConsoleWatcher  console = new ConsoleWatcher();

    private int exitCode;
    private Idl idl;

    @Before
    public void setUp() {
        idl = new Idl() {
            {
                setWorkingDir(tmp.getRoot().toPath());
            }

            @Override
            protected void exit(int i) {
                exitCode = i;
            }
        };
    }

    @Test
    public void testHelp() {
        idl.execute("help");

        assertThat(exitCode, is(0));
        assertThat(console.error(), is(""));
        assertThat(console.output(), is("IDL replacement by morimekta - " + IdlUtils.versionString() + "\n" +
                                        "Usage: idl [-hV] [--verbose] [-R NAME] [--pwd DIR] [--cache DIR] [-r REPO] [-e REPO] cmd [...]\n" +
                                        "\n" +
                                        " --help (-h, -?)        : Show help\n" +
                                        " --version (-V)         : Show program version\n" +
                                        " --verbose              : Show verbose exceptions\n" +
                                        " --remote (-R) NAME     : The remote name or URL to use for repository naming\n" +
                                        " --pwd DIR              : The local git repository root directory (default: .)\n" +
                                        " --cache DIR            : The repository cache directory\n" +
                                        "                          (default: ~/.cache/idltool)\n" +
                                        " --repository (-r) REPO : The main registry git repository\n" +
                                        " --extra-repository (-e) REPO : Extra registry repositories\n" +
                                        " cmd                    : The IDL command to run.\n" +
                                        "\n" +
                                        "Available Commands:\n" +
                                        "\n" +
                                        " help    : Show help information.\n" +
                                        " list    : List available remotes.\n" +
                                        " status  : List status changes to fetched remotes.\n" +
                                        " diff    : List file changes to fetched remotes.\n" +
                                        " update  : Update remote repositories.\n" +
                                        " fetch   : Fetch a remote repository.\n" +
                                        " publish : Publish local IDL to remote.\n"));
    }
}
