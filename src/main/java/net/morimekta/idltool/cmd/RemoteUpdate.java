/*
 * Copyright 2017 (c) Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.idltool.cmd;

import net.morimekta.console.args.ArgumentParser;
import net.morimekta.idltool.IdlTool;
import net.morimekta.idltool.IdlUtils;
import net.morimekta.idltool.git.Repository;
import net.morimekta.idltool.meta.Meta;
import net.morimekta.idltool.meta.Remote;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Map;

/**
 * Interactively manage branches.
 */
public class RemoteUpdate extends Command {
    public RemoteUpdate(ArgumentParser parent) {
        super(parent);
    }

    @Override
    public ArgumentParser makeParser() {
        return new ArgumentParser(getParent(), "update",
                                  "Update all remotes from repository. This command will always " +
                                  "update to latest version of all thrift files.");
    }

    @Override
    public void execute(IdlTool idlTool) throws IOException {
        Meta localMeta = idlTool.getLocalMeta();
        Meta._Builder localMetaBuilder = localMeta.mutate();

        for (Map.Entry<String, Remote> remoteEntry : localMeta.getRemotes().entrySet()) {
            String remoteName = remoteEntry.getKey();
            if (idlTool.getLocalRemoteName().equals(remoteName)) {
                continue;
            }

            Repository repository = idlTool.getRemoteRepository(remoteName);
            Remote remoteRemote = repository.getMeta().getRemotes().get(remoteName);

            Path remoteFilesLocation = repository.remoteFilesLocation(remoteName, false);
            Path localFilesLocation = idlTool.getLocal().remoteFilesLocation(remoteName, true);

            for (String file : remoteRemote.getShasums().keySet()) {
                Path source = remoteFilesLocation.resolve(file);
                Path target = localFilesLocation.resolve(file);
                Files.createDirectories(target.getParent());
                try {
                    Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    throw new IOException("Unable to write " + target.getFileName() + " to " + target.getParent(), e);
                }
            }

            Remote localRemote = idlTool.getLocal().buildRemote(remoteName);
            for (String file : localRemote.getShasums().keySet()) {
                if (!remoteRemote.getShasums().containsKey(file)) {
                    Path target = localFilesLocation.resolve(file);
                    Files.delete(target);
                }
            }

            localMetaBuilder.putInRemotes(remoteName, remoteRemote);

            long version = Math.max(localMetaBuilder.getVersion(), remoteRemote.getVersion());
            localMetaBuilder.setVersion(version);
            localMetaBuilder.setTime(IdlUtils.formatAgo(version));
        }

        idlTool.getLocal().putMeta(localMetaBuilder.build());
    }
}
