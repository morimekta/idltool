package net.morimekta.idltool.git;

import net.morimekta.util.concurrent.ProcessExecutor;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

public class RepositoryCache {
    private final Path cacheDir;
    private final Map<String, Repository> cache;

    public RepositoryCache(Path cacheDir) {
        this.cacheDir = cacheDir;
        this.cache = new HashMap<>();
    }

    public Repository getRepository(String repositoryUrlIsh) throws IOException {
        try {
            return cache.computeIfAbsent(repositoryUrlIsh, r -> {
                try {
                    if (!Files.exists(cacheDir)) {
                        Files.createDirectories(cacheDir);
                    }

                    byte[] sha1 = DigestUtils.sha1(repositoryUrlIsh.getBytes(UTF_8));
                    String b64 = Base64.getUrlEncoder().withoutPadding().encodeToString(sha1);
                    Path dir = cacheDir.resolve(b64);

                    Repository repo;
                    if (!Files.exists(dir)) {
                        ProcessExecutor executor = new ProcessExecutor(
                                "git", "clone", repositoryUrlIsh, b64);
                        executor.setDirectory(dir.getParent().toFile());
                        executor.setDeadlineMs(60000L);
                        if (executor.call() != 0) {
                            throw new IOException("Unable to clone " + repositoryUrlIsh + ": " + executor.getError().split("\r?\n")[0]);
                        }
                        repo = new Repository(dir);
                    } else {
                        repo = new Repository(dir);
                        repo.fetch().add();
                    }
                    repo.reset();
                    return repo;
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            });
        } catch (UncheckedIOException e) {
            throw e.getCause();
        }
    }
}
