/*
 * Copyright 2017 (c) Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.idltool.cmd;

import net.morimekta.console.args.ArgumentParser;
import net.morimekta.idltool.IdlTool;
import net.morimekta.idltool.IdlUtils;
import net.morimekta.idltool.git.Repository;
import net.morimekta.idltool.git.Status;
import net.morimekta.idltool.meta.Meta;
import net.morimekta.idltool.meta.Remote;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 * Interactively manage branches.
 */
public class RemotePublish extends Command {
    public RemotePublish(ArgumentParser parent) {
        super(parent);
    }

    @Override
    public ArgumentParser makeParser() {
        ArgumentParser parser = new ArgumentParser(getParent(), "publish",
                                                   "Update published IDL for the current repository to latest version.");

        return parser;
    }

    @Override
    public void execute(IdlTool idlTool) throws IOException {
        String remoteName = idlTool.getLocalRemoteName();

        Repository local = idlTool.getLocal();
        Path localFilesLocation = local.remoteFilesLocation(remoteName, false);
        if (!Files.exists(localFilesLocation)) {
            return;
        }
        Remote localRemote = local.buildRemote(remoteName);
        if (localRemote.getShasums().isEmpty()) {
            return;
        }

        Repository repository = idlTool.getRemoteRepository(remoteName);
        Path remoteFilesLocation = repository.remoteFilesLocation(remoteName, true);

        for (String file : localRemote.getShasums().keySet()) {
            Path target = remoteFilesLocation.resolve(file);
            Path source = localFilesLocation.resolve(file);
            Files.createDirectories(target.getParent());
            try {
                Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                throw new IOException("Unable to write " + target.getFileName() + " to " + target.getParent(), e);
            }
        }

        Remote remoteRemote = repository.buildRemote(remoteName);
        for (String file : remoteRemote.getShasums().keySet()) {
            if (!localRemote.getShasums().containsKey(file)) {
                Path path = remoteFilesLocation.resolve(file);
                Files.delete(path);
            }
        }

        // check for possibly no-op changes.
        Status status = repository
                .add()
                .getUpdatedStatus();
        if (status.files.isEmpty()) {
            return;
        }

        Meta remoteMeta = repository.getMeta();

        Meta._Builder remoteMetaBuilder = remoteMeta.mutate();
        remoteMetaBuilder.mutableRemotes().put(remoteName, localRemote);
        remoteMetaBuilder.setVersion(localRemote.getVersion());
        remoteMetaBuilder.setTime(IdlUtils.formatAgo(localRemote.getTime()));

        repository.putMeta(remoteMetaBuilder.build());

        repository.add()
                .commit("Updating " + remoteName + " to latest version")
                .push();
    }
}
